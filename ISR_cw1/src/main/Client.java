package main;
import java.net.InetAddress;

public class Client {
	private InetAddress Adres;
	private int Port;
	
	public Client(InetAddress adres, int port) {
		Adres = adres;
		Port = port;
	}
	
	public InetAddress getAdres() {
		return Adres;
	}
	
	public int getPort() {
		return Port;
	}
}
