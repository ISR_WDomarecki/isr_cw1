package main;
import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;

public class UDPClient {
	private InetAddress host;
	private int port;
	private DatagramSocket aSocket;
	
	public UDPClient(String nickname) {
		try {
			aSocket = new DatagramSocket();
			new Listener(aSocket);
		}catch (SocketException e) {
			System.out.println(e.getMessage());
		}
	}

	static private class Listener extends Thread{
		DatagramSocket socketRec;
		
		public Listener(DatagramSocket Socket){
			socketRec = Socket;
			this.start();
		}
		
		public void run() {
			try {
				while(true) {
					byte[] buffer = new byte[2014];
					DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
					socketRec.receive(reply);
					String msg = new String(reply.getData());
					System.out.println(msg);
				}
			}catch(IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}
	
	private void sendMessage(String message) {
		try {
			DatagramPacket msg = new DatagramPacket(message.getBytes(), message.length(),
					host, port);
			aSocket.send(msg);
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void runClient() {
		Scanner scan = new Scanner(System.in);
		while(true) {
			System.out.println("Enter your message: ");
			String msg = scan.next();
			sendMessage(msg);
		}
	}
	
	public static void main(String[] args){
		if (args.length <1) {
			System.out.println("This program requires an argument");
			System.exit(-1);
		}
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter your nickname: ");
		String name = scan.next();
		UDPClient client = new UDPClient(name);
		try {
			client.host = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		client.port = Integer.parseInt(args[0]);
		client.runClient();
	}
	
}
