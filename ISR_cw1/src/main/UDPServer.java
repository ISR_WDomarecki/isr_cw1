package main;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

class ServerFunctions{
	private Map<String, Client> users = new HashMap<>();
	private DatagramSocket aSocket;
	
	public ServerFunctions(int port) {
		try {
			aSocket = new DatagramSocket(port);
		} catch (SocketException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void sendMessage(String answer, Client receiver) {
		DatagramPacket message = new DatagramPacket(answer.getBytes(), answer.length(),
				receiver.getAdres(), receiver.getPort());
		try {
			aSocket.send(message);
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void readMessage(DatagramPacket p) {
		String msg = new String(p.getData()).trim();
		String[] tab = msg.split("\\|");
		String command = tab[0];
		String answer;
		
		switch(command) {
		case "?":
			Client sender1 = new Client(p.getAddress(), p.getPort());
			if(tab.length!=1) {
				answer = "'?' requires no other arguments";
			}else {
				StringBuffer names_buff = new StringBuffer("");
				for (String nick : users.keySet())
					names_buff.append(nick + " ");
				answer = msg + "|" + names_buff.toString().trim();
			}
			sendMessage(answer, sender1); 
			break;
			
		case "+":
			Client client = new Client(p.getAddress(), p.getPort());
			if(tab.length!=2) {
				answer = "'+' requires one other arguments";
			}else {
				if(users.containsKey(tab[1])) {
					answer = "username already exists";
				}else {
					users.put(tab[1], client);
					answer = msg + "|OK";
				}
			}
			sendMessage(answer, client);
			break;
			
		case "-":
			Client remover = new Client(p.getAddress(), p.getPort());
			if(tab.length!=2) {
				answer = "'-' requires one other arguments";
			}else {
				if(!users.containsKey(tab[1])) {
					answer = "username doesn't exist";
				}else {
					users.remove(tab[1]);
					answer = msg + "|OK";
				}
			}
			sendMessage(answer, remover);
			break;
			
		case "!":
			Client sender2 = new Client(p.getAddress(), p.getPort());
			Client receiver = users.get(tab[1]);
			if(tab.length!=3) {
				answer = "'-' requires two other arguments";
			}else {
				if(!users.containsKey(tab[1])) {
					answer = "username doesn't exist";
				}else {
					answer = msg + "|OK";
					sendMessage(tab[2], receiver);
				}
			}
			sendMessage(answer, sender2);
			
		default:
			answer = "command not recognized";
		}
	}
	
	public void runServer() {
		try {
			while(true) {
				byte[] buffer = new byte[1024];
				DatagramPacket request = new DatagramPacket(buffer, buffer.length);
				aSocket.receive(request);
				readMessage(request);
			}
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
}


public class UDPServer {

	public static void main(String[] args) {
		if(args.length < 1) {
			System.out.println("This program requires arguments");
			System.exit(-1);
		}
		ServerFunctions server = new ServerFunctions(Integer.parseInt(args[0]));
		server.runServer();
	}
	
}